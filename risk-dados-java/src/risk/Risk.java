/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package risk;

import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author cisco
 */
public class Risk {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner s = new Scanner(System.in);
//        String linea = s.nextLine();
//        String[] resultados = linea.split(" ");
        String linea = s.nextLine();

        String[] datos = linea.trim().split(" ");
        int tropasDefensa = Integer.parseInt(datos[0]);
        int tropasAtaque = Integer.parseInt(datos[1]);
        int numeroOleadas = Integer.parseInt(datos[2]);
        int dadosDefensa = Integer.parseInt(datos[3]);
        int dadosAtaque = Integer.parseInt(datos[4]);

        //funciona: recoge datos, los convierte en int y los mete en el array, y cambia valores si son altos o bajos
        System.out.println("tiradas dados");
        String linea2 = s.nextLine();
        String[] resultadosDados = linea2.trim().split(" ");

        for (; numeroOleadas > 0; numeroOleadas--) {
            int[] listaResultadoDados = new int[resultadosDados.length];
            for (int i = 0; i < listaResultadoDados.length; i++) {
                listaResultadoDados[i] = Integer.parseInt(resultadosDados[i]);
                if (listaResultadoDados[i] > 6) {
                    listaResultadoDados[i] = 6;
                } else if (listaResultadoDados[i] <= 0) {
                    listaResultadoDados[i] = 1;
                }
            }
            int mismasTropasQueDadosAtaque = Math.min(tropasAtaque, dadosAtaque);
            int mismasTropasQueDadosDefensa = Math.min(tropasDefensa, dadosDefensa);

            int[] resultadosAtaque = new int[mismasTropasQueDadosAtaque];
            int[] resultadosDefensa = new int[mismasTropasQueDadosDefensa];

            int posicionDefensa = 0;
            int posicionAtaque = 0;
            int[] clonlistaResultados = {};
            for (int i = 0; i < resultadosDados.length - 1; i++) {
                try {
                    if (i % 2 == 0 || i == 0) {
                        resultadosDefensa[posicionDefensa] = listaResultadoDados[i];
                        ++posicionDefensa;
                    } else {
                        resultadosAtaque[posicionAtaque] = listaResultadoDados[i];
                        ++posicionAtaque;
                    }
                } catch (Exception e) {
                    continue;
                }
                if (posicionAtaque > posicionDefensa) {
                    clonlistaResultados = Arrays.copyOfRange(listaResultadoDados, posicionAtaque, listaResultadoDados.length - 1);
                    for (int j = 0; j < clonlistaResultados.length; j++) {
                        listaResultadoDados[j] = clonlistaResultados[j];
                        System.out.println(Arrays.toString(listaResultadoDados));
                    }
                } else if (posicionDefensa > posicionAtaque) {
                    clonlistaResultados = Arrays.copyOfRange(listaResultadoDados, posicionDefensa, listaResultadoDados.length - 1);
                    for (int j = 0; j < clonlistaResultados.length; j++) {
                        listaResultadoDados[j] = clonlistaResultados[j];
                        System.out.println(Arrays.toString(listaResultadoDados));
                    }
                }
            }

            System.out.println("Defensa " + Arrays.toString(resultadosDefensa));
            System.out.println("Ataque " + Arrays.toString(resultadosAtaque));
            System.out.println("clon Listado de tiradas: " + Arrays.toString(clonlistaResultados));
            for (int i = 0; i < clonlistaResultados.length; i++) {
                listaResultadoDados[i] = clonlistaResultados[i];
            }
            System.out.println("lista resultados: " + listaResultadoDados.length + " " + Arrays.toString(listaResultadoDados));

            System.out.println("oleada nº" + numeroOleadas);
        }
    }
}
